this pushes a new page onto the view stack

  void selectCategory(BuildContext ctx) {
    Navigator.of(ctx).push(
      MaterialPageRoute(
        builder: (_) {
          return CategoryMealsScreen(id, title); //<-- new page
        },
      ),
    );
  }
