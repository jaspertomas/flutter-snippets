import 'dart:convert';



  Futurevoid addProduct(Product product) async {
    const url = 'httpsflutter-update.firebaseio.comproducts.json';
    try {
      final response = await http.post(
        url,
        body json.encode({
          'title' product.title,
          'description' product.description,
          'imageUrl' product.imageUrl,
          'price' product.price,
          'isFavorite' product.isFavorite,
        }),
      );
      final newProduct = Product(
        title product.title,
        description product.description,
        price product.price,
        imageUrl product.imageUrl,
        id json.decode(response.body)['name'],
      );
      _items.add(newProduct);
       _items.insert(0, newProduct);  at the start of the list
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  Futurevoid updateProduct(String id, Product newProduct) async {
    final prodIndex = _items.indexWhere((prod) = prod.id == id);
    if (prodIndex = 0) {
      final url = 'httpsflutter-update.firebaseio.comproducts$id.json';
      await http.patch(url,
          body json.encode({
            'title' newProduct.title,
            'description' newProduct.description,
            'imageUrl' newProduct.imageUrl,
            'price' newProduct.price
          }));
      _items[prodIndex] = newProduct;
      notifyListeners();
    } else {
      print('...');
    }
  }

  Futurevoid deleteProduct(String id) async {
    final url = 'httpsflutter-update.firebaseio.comproducts$id.json';
    final existingProductIndex = _items.indexWhere((prod) = prod.id == id);
    var existingProduct = _items[existingProductIndex];
    _items.removeAt(existingProductIndex);
    notifyListeners();
    final response = await http.delete(url);
    if (response.statusCode = 400) {
      _items.insert(existingProductIndex, existingProduct);
      notifyListeners();
      throw HttpException('Could not delete product.');
    }
    existingProduct = null;
  }