
    @override
    void dispose() {
      _imageUrlFocusNode.removeListener(_updateImageUrl);
      _priceFocusNode.dispose();
      _descriptionFocusNode.dispose();
      _imageUrlController.dispose();
      _imageUrlFocusNode.dispose();
      super.dispose();
    }