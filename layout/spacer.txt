
This creates an invisible element
                      SizedBox(
                        width: 6,
                      ),


This consumes all available space
, useful for controlling spacing in Rows and Columns 
where main/cross axis spacing distribution is specified
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween, <------
                children: <Widget>[
                  Text(
                    'Total',
                    style: TextStyle(fontSize: 20),
                  ),
                  Spacer(),  <-----maximize space here-----
                  Chip(
                    label: Text(
                      '\$${cart.totalAmount.toStringAsFixed(2)}',
                      style: TextStyle(
                        color: Theme.of(context).primaryTextTheme.title.color,
                      ),
                    ),
                    backgroundColor: Theme.of(context).primaryColor,
                  ),
