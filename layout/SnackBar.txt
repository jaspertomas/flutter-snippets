SnackBar is like Toast in android


              Scaffold.of(context).hideCurrentSnackBar();

---------------------------------------

              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text(
                    'Added item to cart!',
                  ),
                  duration: Duration(seconds: 2),
                  action: SnackBarAction(
                    label: 'UNDO',
                    onPressed: () {
                      cart.removeSingleItem(product.id);
                    },
                  ),
                ),
              );


---------FOR FUTURES, PUT CONTEXT IN A VARIABLE---------------
    final scaffold = Scaffold.of(context);

                  scaffold.showSnackBar(
                    SnackBar(
                      content: Text('Deleting failed!', textAlign: TextAlign.center,),
                    ),
                  );
