pubspec:

dependencies:
  flutter:
    sdk: flutter
  provider: ^3.0.0

-----instance, only one is needed, can be accessed in many places---
-----put this in the 'higher' widget or single source of truth-----
-----use this if the value is part of a list or grid that hides items that are not displayed-----------
ChangeNotifierProvider.value(
            value: products[i],
            child: ProductItem(), 
          ),
------different syntax, dont use in lists-----
ChangeNotifierProvider(
            builder: (c) => products[i],
            child: ProductItem(), 
            create: (BuildContext context) {},
          ),

------or for multiple providers-----          
import 'package:provider/provider.dart';
import './providers/products.dart';

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Products(),
        ),
        ChangeNotifierProvider.value(
          value: Cart(),
        ),
      ],
      child: MaterialApp(...

---------provider of list of products--------------------
//providers/products.dart
import 'package:flutter/material.dart';

import './product.dart';

class Products with ChangeNotifier {
  List<Product> _items = [
    Product(
      id: 'p1',
      title: 'Red Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
          'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
    ),
  ];
  // var _showFavoritesOnly = false;

  List<Product> get items {
    // if (_showFavoritesOnly) {
    //   return _items.where((prodItem) => prodItem.isFavorite).toList();
    // }
    //return a copy, prevent direct editing of values
    //only notifylistener is allowed to update values
    return [..._items];
  }

  List<Product> get favoriteItems {
    return _items.where((prodItem) => prodItem.isFavorite).toList();
  }

  Product findById(String id) {
    return _items.firstWhere((prod) => prod.id == id);
  }

  // void showFavoritesOnly() {
  //   _showFavoritesOnly = true;
  //   notifyListeners();
  // }

  // void showAll() {
  //   _showFavoritesOnly = false;
  //   notifyListeners();
  // }

  void addProduct() {
    // _items.add(value);
    notifyListeners();
  }
}


-----------------provider of a single product, attached to model--------------------
import 'package:flutter/foundation.dart';

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite;

  Product({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.price,
    @required this.imageUrl,
    this.isFavorite = false,
  });

  void toggleFavoriteStatus() {
    isFavorite = !isFavorite;
    //listening widgets only updated once notifylisteners is called
    notifyListeners();
  }
}
-----------------------
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/products.dart';

class ProductsGrid extends StatelessWidget {
  final bool showFavs;

  ProductsGrid(this.showFavs);

  @override
  Widget build(BuildContext context) {
    final productsData = Provider.of<Products>(context);
    final products = showFavs ? productsData.favoriteItems : productsData.items;
    return GridView.builder(...


----ALTERNATE SYNTAX ALLOWING FOR SPECIALIZED CONSUMPTION OF DATA----
When provided data changes,
This updates only the small part of the widget surrounded by Consumer
This example rebuilds only the icon, turning favorite on and off
          IconButton(
            icon: Consumer<Product>(
              builder: (ctx, product, child) => Icon(
                product.isFavorite ? Icons.favorite : Icons.favorite_border,
              ),
            ),
            color: Theme.of(context).accentColor,
            onPressed: () {
              product.toggleFavoriteStatus();
            },
          ),

The child thing is an advanced option, for optimization
it's so that you don't have to rebuild the children of your consumer widget
          Consumer<Cart>(
            builder: (_, cart, ch) => Badge(
                  child: ch,
                  value: cart.itemCount.toString(),
                ),
            child: IconButton(  //<--- this is applied to ch above
              icon: Icon(
                Icons.shopping_cart,
              ),
              onPressed: () {},
            ),
          ),

---------CONSTANTS CAN DO IT LIKE THIS - THESE CANNOT CHANGE-------------
-------do this if you don't want to listen for changes - like for CONSTANTS. -------
Provider<String>(builder: (ctx) => 'Hi, I am a text!', child: ...);
print(Provider.of<String>(context)); // prints 'Hi, I am a text!'; does never update!

The widget will never be rebuilt. This is equal to 'final' modifier
Provider.of<Products>(
      context,
      listen: false, //<--
    )