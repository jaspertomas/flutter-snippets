class Meal {
  final bool isVegetarian;

  //named constructor
  const Meal({
    @required this.isVegetarian,
  });
  //usage: Meal({isVegetarian:true})

  //positional constructor
  const Meal(
    this.isVegetarian, //?
  );
  //usage: Meal(true)
}
---SHORT CUT CONSTRUCTOR -----------------
This automatically sets model properties

class ProductDetailScreen extends StatelessWidget {
  final String title;
  final double price;

  ProductDetailScreen(this.title, this.price);
